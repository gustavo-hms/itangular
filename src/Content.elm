module Content exposing (Area, areas, home, portfolio)


home =
    """
A Itangular Consultoria e Projetos é uma empresa de engenharia civil, que possui duas áreas de atuação principais:
- desenvolvimento de projetos, desde o arquitetônico e estrutural (concreto e metálica), até fundações e contenções, desde a fase de concepção/projeto básico até o detalhamento final, respeitando as normas brasileiras vigentes;
- planejamento e controle tradicional do canteiro de obras, além da opção de uso das  metodologias da *lean construction*, objetivando a redução de custos e prazo de obra, e o aumento do lucro para a empresa.
"""


type alias Area =
    { header : String
    , content : String
    , images :
        List
            { src : String
            , description : String
            }
    }


areas : List Area
areas =
    [ { header = "Projeto de estruturas de concreto armado"
      , content = "Concepção e detalhamento de projetos de estruturas de concreto armado, com respectivos memoriais de cálculo, de acordo com a norma brasileira NBR 6118, considerando as necessidades arquitetônicas e as especificações do cliente."
      , images =
            [ { src = "/img/Projeto de estruturas de concreto armado 1.png"
              , description = "Um projeto de estruturas de concreto armado"
              }
            , { src = "/img/Projeto de estruturas de concreto armado 2.png"
              , description = "Outro projeto de estruturas de concreto armado"
              }
            ]
      }
    , { header = "Projeto de estruturas metálicas"
      , content = "Projetos de estrutura metálica e *light steel frame*, com o memorial de cálculo, considerando a concepção estrutural e o detalhamento, com base nas normas NBR 8800 e NBR 15253."
      , images =
            [ { src = "/img/Projeto de estruturas metálicas 1.png"
              , description = "Um projeto de estruturas metálicas"
              }
            , { src = "/img/Projeto de estruturas metálicas 2.png"
              , description = "Outro projeto de estruturas metálicas"
              }
            , { src = "/img/Projeto de estruturas metálicas 3.png"
              , description = "Outro projeto de estruturas metálicas"
              }
            ]
      }
    , { header = "Projeto de fundação"
      , content = "Projetos de fundação superficial (sapata ou radier) ou profunda (estaca ou tubulão), considerando as características de solo e as cargas da estrutura a ser suportada, de acordo com a NBR 6122."
      , images =
            [ { src = "/img/Projeto de fundação 1.png"
              , description = "Um projeto de fundação"
              }
            , { src = "/img/Projeto de fundação 2.png"
              , description = "Outro projeto de fundação"
              }
            , { src = "/img/Projeto de fundação 3.png"
              , description = "Mais um projeto de fundação"
              }
            ]
      }
    , { header = "Projeto de estruturas de contenção"
      , content = "Projetos de forma e armadura de muros de arrimo em fundação direta ou profunda, com a respectiva memória de cálculo."
      , images =
            [ { src = "/img/Projeto de estruturas de contenção 1.png"
              , description = "Um projeto de estruturas de contenção"
              }
            , { src = "/img/Projeto de estruturas de contenção 2.png"
              , description = "Outro projeto de estruturas de contenção"
              }
            ]
      }
    , { header = "Planejamento de obras"
      , content = """
Desenvolvimento do planejamento físico da obra com a opção de contratação dos seguintes serviços:
- desenvolvimento da estrutura analítica do projeto (EAP) e estruturação da rede PERT/CPM para cálculo dos prazos de entrega de serviços e da obra como um todo;
- desenvolvimento de Linhas de Balanço para verificação de interferências entre serviços com relação de precedência e para otimização dos ritmos de trabalho da obra;
- cronograma de Gantt;
- planejamento e implantação de processos puxados no canteiro por meio de técnicas baseadas na *Lean Construction*.
"""
      , images = []
      }
    , { header = "Controle de obras"
      , content = """
Consultoria e estudos para a melhoria do gerenciamento e controle do canteiro de obra, com a opção de contratação dos seguintes serviços:
- estudos de produtividade;
- consultoria para mapeamento dos processos e identificação de gargalos;
- consultoria para implantação de ferramentas e técnicas de organização e controle do canteiro baseadas na *Lean Construction*;
- consultoria para implantação dos princípios da *Lean Construction* na empresa construtora.
"""
      , images = []
      }
    ]


portfolio =
    [ { header = "COMPERJ (Complexo Petroquímico Do Rio De Janeiro)"
      , content = "Primeiro projeto de grande porte que teve a participação dos integrantes da ITANGULAR, no projeto básico para estimativa de quantitativos do empreendimento. A magnitude do empreendimento e o fato de ser um projeto multidisciplinar representou um desafio à parte, havendo a necessidade de estudos com relação à interferência das estruturas, com o auxílio de ferramentas computacionais que permitiam modelar toda a refinaria e, assim, prever pontos de interferência com tubulação, arranjo, metálica, concreto, pavimentação, drenagem, etc."
      , images =
            [ { src = "/img/comperj.png"
              , description = "Refinaria da COMPERJ"
              }
            ]
      }
    , { header = "RNEST (Refinaria Do Nordeste)"
      , content = "Outra refinaria da Petrobrás que contou com a participação dos profissionais da ITANGULAR. Neste projeto, a equipe atuou no detalhamento das estruturas metálicas, de acordo com a norma NBR8800, como pipe-racks e cable-racks. As cargas horizontais das tubulações são pontos a serem destacados, por serem cargas não observadas em estruturas convencionais (que estão contidas nas normas NBR6120 e NBR6123) e que apresentam uma magnitude significativa."
      , images =
            [ { src = "/img/rnest.png"
              , description = "Refinaria da RNEST"
              }
            ]
      }
    , { header = "UFNIII (Unidade De Feritilizantes Nitrogenados III)"
      , content = "Mais um projeto multidisciplinar da Petrobrás em que os profissionais da ITANGULAR participaram, neste caso no detalhamento de estruturas de concreto armado, pré-fabricado e de fundações, respeitando as normas NBR6118, NBR9062 e  NBR6122 . Os desafios encontrados foram os inerentes ao detalhamento, como respeito às normas brasileiras, verificação da taxa de aço e estudo de interferências."
      , images =
            [ { src = "/img/ufniii.png"
              , description = "Planta da UFNIII"
              }
            ]
      }
    , { header = "Estações Botujuru, Comandante Sampaio e Campo Limpo Paulista (CPTM)"
      , content = "Estações de trem da CPTM (Companhia Paulista de Trens Metropolitanos). Nestas três estações, a equipe da ITANGULAR atuou no detalhamento das estruturas de concreto armado, fundação e de estruturas metálicas. A cobertura de uma das estações, em estrutura metálica, tinha que auxiliar na suportação do nível do mezanino. Outra das estações necessitou de muros de arrimo. Destaca-se também a necessidade do trabalho em conjunto outras disciplinas, como arquitetura, hidráulica e elétrica, para a compatibilização dos projetos."
      , images =
            [ { src = "/img/estações.png"
              , description = "Projeto de estações de trem"
              }
            ]
      }
    , { header = "Gerenciamento da Linha 13 (CPTM)"
      , content = "O desafio neste empreendimento foi a verificação de intervenientes e caminhos críticos para que a obra se alinhasse ao cronograma proposto, propondo otimizações e gerando relatórios de avanço físico e financeiro."
      , images =
            [ { src = "/img/linha 13.png"
              , description = "Linha 13 da CPTM"
              }
            ]
      }
    , { header = "Torres de Telecomunicação (ATC)"
      , content = "Verificação estrutural de torre de telecomunicação e suas fundações. Devido às diversas localidades em que as torres estão presentes, modificando o perfil geológico, há uma diversidade considerável nas soluções das fundações a serem adotadas. Quanto às fundações, também pode-se destacar a presença do esforço significativo de tração, algo que geralmente não ocorre em fundações de edificações normais. Com relação às estruturas das torres, destaca-se a necessidade de uma verificação aprimorada dos esforços de vento atuantes, em conformidade com a NBR6123."
      , images =
            [ { src = "/img/torre.png"
              , description = "Projeto de torre da ATC"
              }
            ]
      }
    ]
