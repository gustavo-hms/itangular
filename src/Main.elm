module Main exposing (main)

import Browser exposing (element)
import Browser.Dom as Dom
import Browser.Events exposing (onResize)
import Content
import Element
    exposing
        ( Element
        , centerX
        , centerY
        , clipX
        , clipY
        , column
        , el
        , fill
        , height
        , html
        , image
        , padding
        , paddingEach
        , paddingXY
        , paragraph
        , pointer
        , px
        , rgb
        , rgb255
        , row
        , scrollbarY
        , scrollbars
        , spacing
        , text
        , textColumn
        , width
        )
import Element.Background as Background
import Element.Events as Events
import Element.Font as Font
import Element.Region as Region
import Html
import Html.Attributes exposing (style)
import Markdown
import Task


main : Program () Model Msg
main =
    Browser.document
        { init = \_ -> ( initialModel, Task.perform initialSize Dom.getViewport )
        , view = \model -> { title = "Itangular Consultoria e Projetos", body = [ view model ] }
        , update = update
        , subscriptions = \_ -> onResize Resize
        }



-- Modelo


type alias Model =
    { window : Window
    , section : Section
    }


type Section
    = Home
    | Areas
    | Contact
    | Portfolio


type alias Window =
    { width : Int
    , height : Int
    }


initialModel =
    { window = { width = 800, height = 600 }
    , section = Home
    }



-- Atualização


type Msg
    = ShowHome
    | ShowAreas
    | ShowContact
    | ShowPortfolio
    | Resize Int Int


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    let
        newModel =
            case msg of
                ShowHome ->
                    { model | section = Home }

                ShowAreas ->
                    { model | section = Areas }

                ShowContact ->
                    { model | section = Contact }

                ShowPortfolio ->
                    { model | section = Portfolio }

                Resize width height ->
                    { model | window = { width = width, height = height } }
    in
    ( newModel, Cmd.none )


initialSize : Dom.Viewport -> Msg
initialSize { viewport } =
    let
        width =
            round viewport.width

        height =
            round viewport.height
    in
    Resize width height



-- Vista


view model =
    let
        device =
            Element.classifyDevice model.window
    in
    if device.class == Element.Phone || device.class == Element.Tablet then
        phoneView model

    else
        desktopView model


hr =
    html <|
        Html.hr
            [ style "width" "100%"
            , style "border" "0"
            , style "height" "1px"
            , style "background-image" "linear-gradient(to right, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0))"
            ]
            []


contentFontSize =
    Font.size 16


paddingValue =
    30


markdown =
    html << Markdown.toHtml []



-------------------------------------------------------------------------------
-- Phone


phoneScale =
    round << Element.modular 26 1.25


phoneView model =
    let
        section =
            case model.section of
                Home ->
                    phoneHome

                Areas ->
                    phoneWorkingArea

                Contact ->
                    phoneContact

                Portfolio ->
                    phonePortfolio
    in
    Element.layout [] <|
        column
            [ spacing 30, width fill ]
            [ phoneLogo
            , phoneMenu model.section
            , el [ Font.family [ Font.typeface "Kinnari", Font.typeface "Times New Roman", Font.typeface "Serif" ] ] section
            ]


phoneLogo =
    row
        [ paddingEach { top = 30, right = 30, bottom = 0, left = 30 }
        , width fill

        -- , centerX
        ]
        [ image [ width (fill |> Element.maximum 600) ] { src = "/img/logo-linha.png", description = "Logotipo" }
        ]


phoneMenu section =
    let
        style element =
            if element == section then
                Font.heavy

            else
                Font.light
    in
    column [ width fill, spacing 30 ]
        [ row
            [ spacing 30
            , paddingEach { top = 30, right = 30, bottom = 0, left = 30 }
            , Font.color (rgb255 207 57 0)
            , Font.family [ Font.typeface "Umpush", Font.typeface "Helvetica", Font.typeface "Arial" ]
            , Font.size (phoneScale 2)
            , width fill
            ]
            [ el [ Events.onClick ShowHome, pointer, style Home ] <| text "Início"
            , el [ Events.onClick ShowAreas, pointer, style Areas ] <| text "Atuação"
            , el [ Events.onClick ShowPortfolio, pointer, style Portfolio ] <| text "Projetos"
            , el [ Events.onClick ShowContact, pointer, style Contact ] <| text "Contato"
            ]
        , hr
        ]


phoneHome =
    textColumn
        [ width fill, Font.size (phoneScale 2), centerY, spacing 10, padding 30 ]
        [ paragraph
            []
            [ markdown Content.home ]
        ]


phoneWorkingArea =
    phoneAreas Content.areas


phoneAreas : List Content.Area -> Element.Element msg
phoneAreas list =
    textColumn [ width fill, spacing 60 ] <| List.map phoneArea list


phoneArea : Content.Area -> Element.Element msg
phoneArea data =
    let
        header =
            paragraph
                [ Region.heading 2, Font.variant Font.smallCaps, Font.bold, Font.color (rgb255 115 115 115), paddingXY paddingValue 0 ]
                [ text data.header ]

        content =
            paragraph
                [ paddingXY paddingValue 0 ]
                [ markdown data.content ]

        images =
            row [] <| List.map (image [ width fill ]) data.images
    in
    textColumn [ width fill, Font.size (phoneScale 2), spacing 1 ] [ header, content, images ]


phonePortfolioView list =
    textColumn [ width fill, spacing 40 ] <| List.map phonePortfolioItem list


phonePortfolioItem data =
    let
        header =
            paragraph
                [ Region.heading 2, Font.variant Font.smallCaps, Font.bold, Font.color (rgb255 115 115 115), paddingXY paddingValue 0 ]
                [ text data.header ]

        content =
            paragraph
                [ paddingXY paddingValue 0 ]
                [ markdown data.content ]

        images =
            row [] <| List.map (image [ width fill ]) data.images
    in
    textColumn [ width fill, Font.size (phoneScale 2), spacing 10 ] [ header, images, content ]


phonePortfolio =
    phonePortfolioView Content.portfolio


phoneContact =
    column
        [ Font.size (phoneScale 2), spacing 30, padding paddingValue, centerX, centerY ]
        [ paragraph
            []
            [ paragraph
                [ Font.bold ]
                [ text "Murilo Gabriel Montesião de Sousa" ]
            , paragraph
                []
                [ text "(11) 9 8135-0155" ]
            ]
        , paragraph
            []
            [ paragraph
                [ Font.bold ]
                [ text "André Luiz Vivan" ]
            , paragraph
                []
                [ text "(19) 9 9356-9042" ]
            ]
        , paragraph
            []
            [ text "E-mail: contato@itangular.com.br" ]
        ]



-------------------------------------------------------------------------------
-- Desktop


desktopMenu section =
    let
        style element =
            if element == section then
                Font.heavy

            else
                Font.light
    in
    column [ width fill ]
        [ row
            [ spacing 15
            , paddingEach { top = 10, right = 10, bottom = 0, left = 10 }
            , Font.color (rgb255 207 57 0)
            , Font.family [ Font.typeface "Umpush", Font.typeface "Helvetica", Font.typeface "Arial" ]
            , Font.size 18
            , width fill
            ]
            [ el [ Events.onClick ShowHome, pointer, style Home ] <| text "Início"
            , el [ Events.onClick ShowAreas, pointer, style Areas ] <| text "Atuação"
            , el [ Events.onClick ShowPortfolio, pointer, style Portfolio ] <| text "Projetos"
            , el [ Events.onClick ShowContact, pointer, style Contact ] <| text "Contato"
            ]
        , hr
        ]


desktopHome =
    textColumn
        [ width fill, Font.size 20, centerY, spacing 10, padding 60 ]
        [ paragraph
            []
            [ markdown Content.home ]
        ]


desktopWorkingArea imagePadding =
    desktopAreas imagePadding Content.areas


desktopAreas : Bool -> List Content.Area -> Element.Element msg
desktopAreas imagePadding list =
    textColumn [ width fill, spacing 60 ] <| List.map (desktopArea imagePadding) list


desktopArea : Bool -> Content.Area -> Element.Element msg
desktopArea imagePadding data =
    let
        header =
            paragraph
                [ Region.heading 2, Font.variant Font.smallCaps, Font.bold, Font.size 20, Font.color (rgb255 115 115 115), paddingXY 20 0 ]
                [ text data.header ]

        content =
            paragraph
                [ paddingXY 20 0 ]
                [ markdown data.content ]

        images =
            row [] <| List.map (image [ width fill, padding 20 ]) data.images
    in
    textColumn [ width fill, Font.size 20, spacing 1 ] [ header, content, images ]


desktopPortfolioView imagePadding list =
    textColumn [ width fill, spacing 40 ] <| List.map (desktopPortfolioItem imagePadding) list


desktopPortfolioItem imagePadding data =
    let
        header =
            paragraph
                [ Region.heading 2, Font.variant Font.smallCaps, Font.bold, Font.size 20, Font.color (rgb255 115 115 115), paddingXY 20 0 ]
                [ text data.header ]

        content =
            paragraph
                [ paddingXY 20 0 ]
                [ markdown data.content ]

        images =
            row [] <| List.map (image [ width (fill |> Element.maximum 850), centerX, padding 20 ]) data.images
    in
    textColumn [ width fill, Font.size 20, spacing 1 ] [ header, images, content ]


desktopPortfolio imagePadding =
    desktopPortfolioView imagePadding Content.portfolio



-- tabletView model =
--     let
--         section =
--             case model.section of
--                 Home ->
--                     home
--                 Areas ->
--                     workingArea FalKinnari
--                 Contact ->
--                     contact
--                 Portfolio ->
--                     portfolio False
--     in
--     Element.layout [] <|
--         column
--             [ width fill, height <| px model.window.height ]
--             [ menu model.section
--             , column
--                 [ width fill
--                 , height fill
--                 , clipY
--                 , Background.uncropped "/img/fundo-borrado.png"
--                 ]
--                 [ logoView
--                 , section
--                 ]
--             ]


desktopView model =
    let
        section =
            case model.section of
                Home ->
                    desktopHome

                Areas ->
                    desktopWorkingArea True

                Contact ->
                    desktopContact

                Portfolio ->
                    desktopPortfolio True
    in
    Element.layout
        [ Font.family
            -- [ Font.typeface "Umpush" ]
            [ Font.typeface "Kinnari", Font.typeface "Times New Roman", Font.typeface "Serif" ]

        -- [ Font.typeface "Garuda" ]
        -- [ Font.typeface "Loma" ]
        ]
    <|
        row
            [ width <| px model.window.width, height <| px model.window.height ]
            [ column
                [ width <| Element.fillPortion 2
                , height <| px model.window.height
                , Element.behindContent <|
                    image
                        [ clipX, height <| px model.window.height ]
                        { src = "/img/fundo-borrado.jpg", description = "Fundo" }
                ]
                [ el
                    [ centerX
                    , centerY
                    ]
                    desktopLogoView
                ]
            , column
                [ width <| Element.fillPortion 3
                , spacing 15
                , clipY
                , scrollbarY
                , Background.color <| rgb 1 1 1
                , height <| px model.window.height
                , Element.alignTop
                ]
                [ desktopMenu model.section
                , section
                ]
            ]


desktopLogoView =
    column
        [ height fill, width fill, centerX, centerY ]
        [ image [ centerX, width <| px 300 ] { src = "/img/logo simples.png", description = "Logotipo" }
        , el
            [ Font.size 60
            , Font.color (rgb 1 1 1)
            , Font.bold
            , centerX
            ]
          <|
            text "Itangular"
        , el
            [ Font.size 25
            , Font.color (rgb 1 1 1)
            , centerX
            ]
          <|
            text "Consultoria e projetos"
        ]


desktopContact =
    column
        [ Font.size 20, spacing 30, padding 20, centerX, centerY ]
        [ paragraph
            []
            [ paragraph
                [ Font.bold ]
                [ text "Murilo Gabriel Montesião de Sousa" ]
            , paragraph
                []
                [ text "(11) 9 8135-0155" ]
            ]
        , paragraph
            []
            [ paragraph
                [ Font.bold ]
                [ text "André Luiz Vivan" ]
            , paragraph
                []
                [ text "(19) 9 9356-9042" ]
            ]
        , paragraph
            []
            [ text "E-mail: contato@itangular.com.br" ]
        ]
